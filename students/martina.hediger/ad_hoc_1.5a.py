#Martina Hediger, 15.2.22 übung 1.5a

from turtle import *

laenge = numinput("Dreieck", "Bitte Seitenlänge angeben:")

pencolor("red")
pensize(5)

#kleinstes Dreieck
filling = textinput("Dreieck", "In welcher Farbe ich das Dreieck ausmalen (engl.)?")
fillcolor(filling)
begin_fill()
left(30)
forward(laenge)
left(120)
forward(laenge)
left(120)
forward(laenge)
left(120)
end_fill()

#mittelgrosses Dreieck
#länge anpassen
laenge = laenge *1.50
filling = textinput("Dreieck", "In welcher Farbe ich das Dreieck ausmalen (engl.)?")
fillcolor(filling)
begin_fill()
right(120)
forward(laenge)
left(120)
forward(laenge)
left(120)
forward(laenge)
left(120)
end_fill()

#grosses Dreieck
#länge anpassen
laenge = laenge *1.50
filling = textinput("Dreieck", "In welcher Farbe ich das Dreieck ausmalen (engl.)?")
fillcolor(filling)
begin_fill()
right(120)
forward(laenge)
left(120)
forward(laenge)
left(120)
forward(laenge)
left(120)
end_fill()