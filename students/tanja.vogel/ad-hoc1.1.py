# Ad hoc 1.1b
# Schreiben Sie einen Befehl, der alle Zahlen von 1 bis 5 addiert, subtrahiert, multipliziert und dividiert
print(1+2+3+4+5)
print(1-2-3-4-5)
print(1*2*3*4*5)
print(1/2/3/4/5)

# Ad hoc 1.1c
# Schreiben Sie einen Befehl, der den Satz wenn fliegen hinter fliegen fliegen fliegen fliegen hinter fliegen ausgibt und dabei so kurz wie möglich ist!
f = "fliegen "
print("wenn", f+ "hinter", 5*f+ "hinterher")

print("... hinter", ("fliegen " * 5) + "hinterher")