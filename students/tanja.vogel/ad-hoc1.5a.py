# Ad-hoc 1.5a

from turtle import *

seite1 = numinput("grünes Dreieck","Bitte geben Sie die Länge der Seite an") # Seitenlänge 100
pensize(5)
pencolor("red")
fillcolor("lime")
begin_fill()
left(30)
forward(seite1)
left(120)
forward(seite1)
left(120)
forward(seite1)
end_fill()

fillcolor("cyan")
seite2 = numinput("blaues Dreieck", "Bitte geben Sie die Länge der Seite an") #Seitenlänge 150
begin_fill()
forward(seite2)
left(120)
forward(seite2)
left(120)
forward(seite2)
end_fill()

fillcolor("yellow")
seite3 = numinput("gelbes Dreieck", "Bitte geben Sie die Länge der Seite an") #Seitenlänge 200
begin_fill()
forward(seite3)
left(120)
forward(seite3)
left(120)
forward(seite3)
end_fill()