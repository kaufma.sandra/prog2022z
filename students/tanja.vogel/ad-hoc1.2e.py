# Ad-hoc 1.2 e
# drei Farbige Dreiecke, gleichmässig verteilt; blue, green, red

from turtle import *

pensize(5)
pencolor("red")
forward(60)
left(120)
forward(60)
left(120)
forward(60)
pencolor("lightgreen")
forward(60)
left(120)
forward(60)
left(120)
forward(60)
pencolor("blue")
forward(60)
left(120)
forward(60)
left(120)
forward(60)
