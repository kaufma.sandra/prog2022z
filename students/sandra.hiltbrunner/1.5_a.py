from turtle import *

laenge = numinput("Dreieck", "Bitte geben Sie die Länge des Dreiecks an")

farbe = textinput("Dreieck", "In welcher Farbe soll ich das Dreieck zeichnen?")
pencolor(farbe)
pensize(5)
fillcolor("lime")

begin_fill()
left(90)
forward(laenge)
right(120)
forward(laenge)
right(120)
forward(laenge)
right(120)
end_fill()

# länge anpassen
laenge = laenge * 2.0
farbe = textinput("Dreieck", "In welcher Farbe soll ich das Dreieck zeichnen?")
pencolor(farbe)
pensize(5)
fillcolor("cyan")

begin_fill()
right(180)
forward(laenge)
left(120)
forward(laenge)
left(120)
forward(laenge)
left(120)
end_fill()

# länge anpassen
laenge = laenge * 3.0
farbe = textinput("Dreieck", "In welcher Farbe soll ich das Dreieck zeichnen?")
pencolor(farbe)
pensize(5)
fillcolor("yellow")

begin_fill()
right(120)
forward(laenge)
left(120)
forward(laenge)
left(120)
forward(laenge)
left(120)
end_fill()
