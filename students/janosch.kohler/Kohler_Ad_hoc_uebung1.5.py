#Ad hoc Übungen 1.5




#1.5 a)
from turtle import *

#input-Zeile jeweils zu Beginn des Dreiecks, dann fragt es vor jedem, wie gross es sein soll.
laenge = numinput("Dreieck", "Wie gross soll das 1. Dreieck sein?")


pencolor("red")
pensize(5)
fillcolor("yellow")
begin_fill()
left(30)
forward(laenge) #Wert laenge wird vom Input, den die Person einträgt, genommen.
left(120)
forward(laenge)
left(120)
forward(laenge)
end_fill()

penup() #musste ich einfügen, damit ich wieder zum Mittelpunkt komme.
left(120)
forward(laenge)
right(120)

laenge = numinput("Dreieck", "Wie gross soll das 2. Dreieck sein?")

fillcolor("cyan")
pendown()
begin_fill()
forward(laenge)
left(120)
forward(laenge)
left(120)
forward(laenge)
end_fill()

laenge = numinput("Dreieck", "Wie gross soll das 3. Dreieck sein?")

right(120) #hatte zuerst den Schlusspfeil an einem anderen Ort als in der Vorlage und musste die Zeichenrichtung des Dreiecks anpassen
fillcolor("lime")
begin_fill()
forward(laenge)
left(120)
forward(laenge)
left(120)
forward(laenge)
end_fill()
