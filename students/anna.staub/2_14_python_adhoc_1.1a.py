#AdHoc Übung 1.1a
#Datum: 14.Februar 2022
#Author: Anna Staub

#Stellen Sie mit der print-Anweisung einen Datensatz aus folgenden einzelnen Bestandteilen zusammen: Name, Vorname, Strasse, Hausnr, PLZ, Wohnort
print('Name')
print('Vorname')
print('Strasse')
print('Hausnr')
print('PLZ')
print('Wohnort')

#oder
print('Bitte füllen Sie Ihre Angaben ein:')
name = input('Ihr Name: ')
vorname = input('Ihr Vorname: ')
strasse = input('Ihre Strasse: ')
hausnr = input('Ihre Hausnummer: ')
plz = input('Ihre Postleitzahl: ')
wohnort = input('Ihr Wohnort: ')

print(name)
print(vorname)
print(strasse)
print(hausnr)
print(plz)
print(wohnort)
