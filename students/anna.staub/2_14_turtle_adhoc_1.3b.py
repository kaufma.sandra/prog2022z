#AdHoc Übungen 1.3b
#Turtle
#Datum: 14.2.2022
#Author: Anna Staub

from turtle import *

#1.3b
pencolor('red')
pensize(5)

fillcolor("cyan")
begin_fill()
lt(40)
fd(100)
rt(90)
fd(100)
rt(90)
fd(100)
rt(90)
fd(100)
end_fill()

fillcolor("yellow")
begin_fill()
rt(160)
fd(100)
rt(90)
fd(100)
rt(90)
fd(100)
rt(90)
fd(100)
end_fill()

fillcolor("magenta")
begin_fill()
rt(160)
fd(100)
rt(90)
fd(100)
rt(90)
fd(100)
rt(90)
fd(100)
end_fill()

fillcolor("blue")
begin_fill()
rt(160)
fd(100)
rt(90)
fd(100)
rt(90)
fd(100)
rt(90)
fd(100)
end_fill()

fillcolor("lime")
begin_fill()
lt(100)
fd(100)
lt(90)
fd(100)
lt(90)
fd(100)
lt(90)
fd(100)
end_fill()



#not finished yet

