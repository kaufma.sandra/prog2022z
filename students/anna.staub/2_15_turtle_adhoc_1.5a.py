#AdHoc Übung 1.5a
#Turtle
#Datum: 15.2.2022
#Author: Anna Staub

from turtle import *
pencolor('red')
pensize(5)

laenge = numinput("Dreieck", "Bitte Seitenlänge angeben: ")
fillcolor('yellow')
begin_fill()
rt(150)
fd(laenge)
rt(120)
fd(laenge)
rt(120)
fd(laenge)
end_fill()

laenge = laenge/2
fillcolor('cyan')
begin_fill()
rt(60)
fd(laenge)
lt(120)
fd(laenge)
lt(120)
fd(laenge)
end_fill()

laenge = laenge/2
fillcolor('lime')
begin_fill()
rt(120)
fd(laenge)
lt(120)
fd(laenge)
end_fill()
lt(120)
fd(laenge)