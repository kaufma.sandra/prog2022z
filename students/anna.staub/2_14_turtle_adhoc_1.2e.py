#AdHoc Übungen 1.2e
#Turtle
#Datum: 14.2.2022
#Author: Anna Staub

from turtle import *

#1.2 e

pensize(5)

pencolor('red')
forward(100)
left(120)
forward(100)
left(120)
forward(100)

pencolor('blue')
right(60)
forward(100)
right(120)
forward(100)
right(120)
forward(100)

pencolor('lightgreen')
right(60)
forward(100)
left(120)
forward(100)
left(120)
forward(100)


