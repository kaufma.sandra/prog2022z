#Datum: 14.02.2022
#Autor: Oliver Dummermuth

#Ad-hoc-Übung: 1.1.a
#Aufgabe: Stellen Sie mit der print-Anweisung einen Datensatz aus folgenden einzelnen Bestandteilen zusammen


#AB DER NÄCHSTEN ZEILE FOLGT CODE
#Variante 1:
print('Name')
print('Vorname')
print('Strasse')
print('Hausnummer')
print('Postleitzahl')
print('Wohnort')

#Variante 2:
print('Teilen Sie uns bitte Ihre Angaben mit.')

name = input('Ihr Name: ')
vorname = input('Ihr Vorname: ')
strasse = input('Ihre Strasse: ')
hausnummer = input('Ihre Hausnummer: ')
postleitzahl = input('Ihre Postleitzahl: ')
wohnort = input('Ihr Wohnort: ')

print(name)
print(vorname)
print(strasse)
print(hausnummer)
print(postleitzahl)
print(wohnort)
