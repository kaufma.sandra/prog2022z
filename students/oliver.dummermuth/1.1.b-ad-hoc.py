#Datum: 14.02.2022
#Autor: Oliver Dummermuth

#Ad-hoc-Übung: 1.1.b
#Aufgabe: Schreiben Sie einen Befehl, der alle Zahlen von 1 bis 5 addiert, subtrahiert, multipliziert und dividiert


#AB DER NÄCHSTEN ZEILE FOLGT CODE
print('1 + 2 + 3 + 4 + 5:',1+2+3+4+5)
print('1 - 2 - 3 - 4 - 5:',1-2-3-4-5)
print('1 * 2 * 3 * 4 * 5:',1*2*3*4*5)
print('1 / 2 / 3 / 4 / 5:',1/2/3/4/5)

