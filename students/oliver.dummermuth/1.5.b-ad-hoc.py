#Datum: 14.02.2022
#Autor: Oliver Dummermuth

#Ad-hoc-Übung: 1.5.b
#Aufgabe: siehe 2_Python_Introduction.pdf, Folie 28


#AB DER NÄCHSTEN ZEILE FOLGT CODE
from turtle import *

laenge = numinput('Eingabefenster', 'Bitte Seitenlänge angeben:')
f = 0.8

pensize(5)
pencolor('red')

begin_fill()
fillcolor('cyan')
right(45)
forward(laenge)
left(90)
forward(laenge)
left(90)
forward(laenge)
left(90)
forward(laenge)
end_fill()

begin_fill()
fillcolor('yellow')
left(15)
forward(laenge*f)
left(90)
forward(laenge*f)
left(90)
forward(laenge*f)
left(90)
forward(laenge*f)
end_fill()

begin_fill()
fillcolor('magenta')
left(15)
forward(laenge*f**2)
left(90)
forward(laenge*f**2)
left(90)
forward(laenge*f**2)
left(90)
forward(laenge*f**2)
end_fill()

begin_fill()
fillcolor('blue')
left(15)
forward(laenge*f**3)
left(90)
forward(laenge*f**3)
left(90)
forward(laenge*f**3)
left(90)
forward(laenge*f**3)
end_fill()

begin_fill()
fillcolor('lime')
left(15)
forward(laenge*f**4)
left(90)
forward(laenge*f**4)
left(90)
forward(laenge*f**4)
left(90)
forward(laenge*f**4)
end_fill()

left(70)



