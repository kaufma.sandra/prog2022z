#Datum: 14.02.2022
#Autor: Oliver Dummermuth

#Ad-hoc-Übung: 1.1.d
#Aufgabe: Schreiben Sie einen Befehl, der den String
#'Hello' rückwärts ausgibt


#Dieser Code führte nicht zum gewünschten Ergebnis:
#print('Hello'[5:0])

#AB DER NÄCHSTEN ZEILE FOLGT CODE
print('Hello'[::-1])
