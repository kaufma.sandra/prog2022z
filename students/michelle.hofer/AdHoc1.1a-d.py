#Adhoc 1.1a
print("Name,","Vorname,","Strasse,","Hausnr.,","PLZ","Wohnort")

#Adhoc 1.1b
print(1+2+3+4+5)
print(1-2-3-4-5)
print(1*2*3*4*5)
print(1/2/3/4/5)
print(1+2+3+4+5, 1-2-3-4-5, 1*2*3*4*5, 1/2/3/4/5) #zusammengehängte Variante

#Adhoc 1.1c
print("wenn fliegen hinter fliegen fliegen fliegen fliegen fliegen hinterher") #ohne strings
print("wenn","fliegen","hinter","fliegen","fliegen","fliegen","fliegen","fliegen","hinterher") #lange string Version
print("wenn fliegen hinter","fliegen "*5+"hinterher") #gekürzte Version

f = "fliegen "
print("wenn", f+ "hinter", 5*f+ "hinterher") #Morena, Version mit Variable

#Adhoc 1.1d
print("Hello"[::-1]) #start und stop leer lassen, nur step ausfüllen, da wir alle Buchstaben rückwärts wollen
help(slice)