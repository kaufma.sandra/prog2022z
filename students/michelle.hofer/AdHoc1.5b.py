from turtle import *
pencolor("red")
pensize(5)
laenge = numinput("Eingabefenster", "Bitte Seitenlänge angeben")
fillcolor("cyan")
begin_fill()
right(40)
forward(laenge)
left(90)
forward(laenge)
left(90)
forward(laenge)
left(90)
forward(laenge)
end_fill()
laenge = numinput("Eingabefenster", "Bitte Seitenlänge angeben")
fillcolor("yellow")
begin_fill()
left(10)
forward(laenge)
left(90)
forward(laenge)
left(90)
forward(laenge)
left(90)
forward(laenge)
end_fill()
laenge = numinput("Eingabefenster", "Bitte Seitenlänge angeben")
fillcolor("magenta")
begin_fill()
left(10)
forward(laenge)
left(90)
forward(laenge)
left(90)
forward(laenge)
left(90)
forward(laenge)
end_fill()
laenge = numinput("Eingabefenster", "Bitte Seitenlänge angeben")
fillcolor("blue")
begin_fill()
left(20)
forward(laenge)
left(90)
forward(laenge)
left(90)
forward(laenge)
left(90)
forward(laenge)
end_fill()
laenge = numinput("Eingabefenster", "Bitte Seitenlänge angeben")
fillcolor("lightgreen")
begin_fill()
left(20)
forward(laenge)
left(90)
forward(laenge)
left(90)
forward(laenge)
left(90)
forward(laenge)
end_fill()