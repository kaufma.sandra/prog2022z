from turtle import *

pencolor("red")
pensize(10)

fillcolor("yellow")
begin_fill()
seitenlaenge = numinput("Eingabefenster", "Bitte Seitenlänge angeben:")
right(90)
forward(seitenlaenge)
left(120)
forward(seitenlaenge)
left(120)
forward(seitenlaenge)
left(180)
forward(seitenlaenge)
end_fill()

# Länge vom blauen Dreieck anpassen
seitenlaenge = seitenlaenge * 0.60

fillcolor("cyan")
begin_fill()
forward(seitenlaenge)
right(120)
forward(seitenlaenge)
right(120)
forward(seitenlaenge)
end_fill()

# Länge vom grünen Dreieck anpassen
seitenlaenge = seitenlaenge * 0.40

fillcolor("lime")
begin_fill()
forward(seitenlaenge)
right(120)
forward(seitenlaenge)
right(120)
forward(seitenlaenge)
right(120)
end_fill()


