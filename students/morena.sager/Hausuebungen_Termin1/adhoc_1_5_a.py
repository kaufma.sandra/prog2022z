#adhoc übung 1.5.a

from turtle import *


speed(8)
laenge = numinput("Dreieck", "Bitte Seitenlänge angeben:")

right(90)
pensize(5)
pencolor("red")
fillcolor("cyan")
begin_fill()
forward(laenge)
left(120)
forward(laenge)
left(120)
forward(laenge)
end_fill()

laenge = laenge * 2
fillcolor("yellow")
begin_fill()
forward(laenge)
left(120)
forward(laenge)
left(120)
forward(laenge)
end_fill()

laenge = laenge / 4
fillcolor("lime")
begin_fill()
forward(laenge)
left(120)
forward(laenge)
left(120)
forward(laenge)
end_fill()

