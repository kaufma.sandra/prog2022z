#adhoc übung 1.2.e
from turtle import *
speed(8)

left(180)
pensize(5)
pencolor("blue")
forward(100)
right(120)
forward(100)
right(120)
forward(100)

left(120)
pencolor("red")
forward(100)
right(120)
forward(100)
right(120)
forward(100)

left(120)
pencolor("lime")
forward(100)
right(120)
forward(100)
right(120)
forward(100)
