# Ad hoc 1.1b

print(1+2+3+4+5)
print(1-2-3-4-5)
print(1*2*3*4*5)
print(1/2/3/4/5)

# Ad hoc 1.1c
f = "fliegen "
print("wenn", f+ "hinter", 5*f+ "hinterher")

print("... hinter", ("fliegen " * 5) + "hinterher")
